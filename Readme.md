# **Flexbox and Grid**
[Part 1 : CSS Flexbox](#css-flexbox)

[Part 2 : CSS Grid](#css-grid)

# **CSS Flexbox**

## Contents :
* Background 
* Basic Concept about CSS Flexbox
* Properties of Flexbox Container
* Properties of Flexbox Items

---

## **Background** 
The Flexbox is an effective layout system to align and distribute space among items in a container, even when their size is unknown changing or both (resizing tabs). It provides flexibility to designers that's why it's called flexible (flex).

---

## **Basic Concept about CSS Flexbox** 

  * **Flexbox Container  :** 
 A Flexbox container is a box within which flex items reside. Here we can define the behaviour of flex items.

* **Flexbox Items  :** 
The direct childs elements of a flex container are called flexbox item.
* **main axis  :**
The main axis of a flex container is the primary axis along which flex items are laid out. By default it's on horizontal direction for row-wise flexbox but if we change to column-wise the primary axis will be in vertical direction.
* **cross axis  :**
Cross axis is the the axis perpendicular to the main axis.
* **main size  :** 
The length of the Flex container along the main axis is called main size.  
* **cross size  :**
The length of the Flex container along the cross axis is called cross size.  
* **main start and main end :**
The new flex items start from main start and move towards main end.
* **cross start and cross end :**
If flex-line has occupied full main size then the new flex line will start at cross start and succesive flex-lines will move towards cross end.


<img src="https://css-tricks.com/wp-content/uploads/2018/11/00-basic-terminology.svg" width="60%" >

---

## **Flexbox Container Properties** 

### **display**
The flex container becomes flexible by setting the display property to flex or inline flex.
```css
.container {
  display: flex; /* or inline-flex*/
}
```

### **flex-direction**
This property defines in which direction the container wants to place the flex items.
* **row (default)**: left to right
* **row-reverse**: right to left
* **column** : top to bottom.
* **column-reverse** : bottom to top

```css
.container {
  flex-direction: row | row-reverse | column | column-reverse;
}
```
<img src="https://css-tricks.com/wp-content/uploads/2018/10/flex-direction.svg" width="60%">

### **flex-wrap**
The flex-wrap property specifies whether the flex items should wrap or not.If the flex-line has full items then next items will wrap according to the given value. 
```css
.container {
  flex-wrap: nowrap | wrap | wrap-reverse;
}
```
<img src="https://css-tricks.com/wp-content/uploads/2018/10/flex-wrap.svg" width="50%">

* **nowrap (default)**: all flex items will be on one line.

 * **wrap**: flex items will wrap onto multiple lines, from top to bottom.

 * **wrap-reverse**: flex items will wrap onto multiple lines from bottom to top.

### **flex-flow**
It is a shorthand property to set both *flex-direction* and *flex-wrap*. default value is *row no-wrap*.
```css
.container {
  display: flex;
  flex-flow: row wrap;
}
```

### **justify-content**
This propert is used to align flex-item along the main axis.
```css
.container {
  justify-content: flex-start | flex-end | center | space-between | space-around | space-evenly ;
}
```
* **flex-start (default)**: It aligns the flex items at the beginning of the container.
* **flex-end**: It aligns the flex items at the end of the container.
* **space-between**: It displays the flex items with equal space between the lines.
* **space-around**: It displays the flex items with space before, between, and after the lines.
* **center**:It will align the items at the center of main axis.
* **space-evenly**: It will distribute equal space between any two items.

<img src="https://css-tricks.com/wp-content/uploads/2018/10/justify-content.svg" width="50%" height="350px">

### **align-items**
This Property is used to align flex items along the cross-axis.
```css
.container {
  align-items: stretch | flex-start | flex-end | center | baseline;
}
```
*  **stretch (default)** : It stretches the flex items to fill the container.
* **flex-start** : It aligns the flex items at the top of the container.
* **flex-end** : It aligns the flex items at the bottom of the container.
* **center** : It aligns the flex items in the middle of the container.
* **baseline** : It aligns the flex items such as their baselines aligns.
<img src="https://css-tricks.com/wp-content/uploads/2018/10/align-items.svg" width="50%" height="330px">

---

## **Flexbox Item Properties** 
The direct child elements of a flex container automatically becomes flex items.

### **order**
This property specifies the order of the flex items. by default it is 0 for an item.
<img src="https://css-tricks.com/wp-content/uploads/2018/10/order.svg" width="50%" height="250px">
```css
.item {
  order: 2; 
}
```

### **flex-grow**
The flex-grow property specifies how much a flex item will grow relative to the rest of the flex items if some free space is available. by default it is 0.
If all items have flex-grow set to 1, then each flex item will grow equal length along main axis.
```css
.item {
  flex-grow: 1;
}
```
<img src="https://css-tricks.com/wp-content/uploads/2018/10/flex-grow.svg" width="50%">

### **flex-shrink**
The flex-shrink property specifies how much a flex item will shrink relative to the rest of the flex items. default value is 1.
```css
.item {
  flex-shrink: 2;
}
```

### **flex-basis**
The flex-basis property sets the initial length of a flex item. It can be a length such as 25%, 10rem,50px, etc.
```css
.item {
  flex-basis: 50px;
}
```

### **flex**
The flex property is a shorthand property for the *flex-grow*, *flex-shrink*, and *flex-basis* properties.
```css
.item {
  flex: 1 1 0;
}
```
### **self-align**
The align-self property specifies the alignment for an individual item of a flex-container.It overrides the default alignment set by the container's *align-items* property.
```css
.item {
  align-self: auto | flex-start | flex-end | center | baseline | stretch;
}
```
<img src="https://css-tricks.com/wp-content/uploads/2018/10/align-self.svg" width="50%" height="150px">

---
---

# **CSS Grid**
## Contents :
* Background 
* Properties of Grid Container
* Properties of Grid Items
---

## **Background**
The CSS Grid Layout offers a grid-based layout system, by allowing to work on both row and column at a time making it possible to design web pages of desired looks.

---
## **Properties of CSS Grid Container**
A container element becomes a grid container when its display property is set to grid or inline-grid.
```css
.container {
  display: grid; /* or inline grid*/
}
```

### **grid-template-columns**
The grid-template-columns property sets the number of columns in a grid layout and also the width of each column.
The value is a space-separated-list, and each value counts as a column of grid container.

If we want our grid layout to contain 4 columns,we should specify the width of the 4 columns. Setting width "auto" to each column will make all columns to have same width.
```css
.container {
  display: grid;
  grid-template-columns: auto auto auto auto; /* 4 columns of equal size*/
}
```
---
### **grid-template-rows**
The grid-template-rows property defines the height of each row.The value is a space-separated-list, where each value defines the height of the respective row.
```css
.container {
  display: grid;
  grid-template-rows: 80px 200px; /* 1st row 80px and 2nd row is 200px of height */
}
```
---
### **justify-content**
It aligns the whole grid horizontlly (row-axis) inside the container. To let the justify-content work the total width of grid must be less than the container's width.
```css
.container {
  display: grid;
  justify-content: space-evenly | space-around | space-between | center | start | end;
}
```
Example:
```css
.container {
  justify-items: start;
}
```
<img src="https://css-tricks.com/wp-content/uploads/2018/11/justify-items-start.svg" width="50%" height="150px">

---
### **align-content**
It aligns the whole grid vertically inside the container. To let the justify-content work the total height of grid must be less than the container's height.

```css
.container {
  display: grid;
  align-content: center | space-evenly | space-around | space-between | start | end;
}
```
Example:
```css
.container {
  align-content: start;    
}
```
<img src="https://css-tricks.com/wp-content/uploads/2018/11/align-content-start.svg" width="50%" height="200px">

---
## **Properties of CSS Grid Items**
---
### grid-column-start, grid-column-end, grid-row-start, grid-row-end
 These properties are used to set the location of a grid Item, grid-column-start/grid-row-start is the line where the item starts, and grid-column-end/grid-row-end is the line where the item ends.
```css
 .item {
  grid-column-start: <number> | <name> | span <number> | span <name> | auto;
  grid-column-end: <number> | <name> | span <number> | span <name> | auto;
  grid-row-start: <number> | <name> | span <number> | span <name> | auto;
  grid-row-end: <number> | <name> | span <number> | span <name> | auto;
}
```
---
 * **line**: can be a number to refer to a numbered grid line, or a name to refer to a named grid line
* **span number**: – the item will span across the provided number of grid tracks
* **span <name>**: – the item will span across until it hits the next line with the provided name
* **auto** – indicates auto-placement, an automatic span, or a default span of one.

example
```css
.item-a {
  grid-column-start: 2;
  grid-column-end: five;
  grid-row-start: row1-start;
  grid-row-end: 3;
}
```

<img src="https://css-tricks.com/wp-content/uploads/2018/11/grid-column-row-start-end-01.svg" width="60%" height="300px">

---
### grid-column, grid-row
It is a Shorthand property for grid-column-start + grid-column-end, and grid-row-start + grid-row-end, respectively.
```css
.item {
  grid-column: <start-line> / <end-line> | <start-line> / span <value>;
  grid-row: <start-line> / <end-line> | <start-line> / span <value>;
}
```
Example;
```css
.item {
  grid-column: 3 / span 2; /* strart from 3rd column and span 2 columns*/
  grid-row: third-line / 4; /* start from 3rd row and end before 4th row*/
}
```
<img src="https://css-tricks.com/wp-content/uploads/2018/11/grid-column-row.svg" width="60%" height="280px">

---
### grid-area
The grid-area property can be used as a shorthand property for the grid-row-start, grid-column-start, grid-row-end and the grid-column-end properties.

Values:

* **name** – a name of designer's choice
* **row-start  / column-start / row-end / column-end** 
* **number**
```css
.item {
  grid-area: <name> | <row-start> / <column-start> / <row-end> / <column-end>;
}
```
Example
```css
.item-d {
  grid-area: 1 / col4-start / last-line / 6;
  /* start at row 1 col 4 and end at last row before col 6. */
}
```
<img src="https://css-tricks.com/wp-content/uploads/2018/11/grid-area.svg" width="60%" height="280px"> 

----
### justify-self
This property aligns a grid item inside a cell along the row axis. This value applies to a grid item inside a single cell.

```css
.item {
  justify-self: start | end | center | stretch;
}
```
Values:
**start** – aligns the grid item to be flush with the start edge of the cell.

**end** – aligns the grid item to be flush with the end edge of the cell.

**center** – (default) aligns the grid item in the center of the cell.

**stretch** – fills the whole width of the cell (this is the default)
Example
```css
.item-a {
  justify-self: start;
}
```
<img src="https://css-tricks.com/wp-content/uploads/2018/11/justify-self-start.svg" width="40%">

```css
.item-a {
  justify-self: end;
}
```
<img src="https://css-tricks.com/wp-content/uploads/2018/11/justify-self-end.svg" width="40%">

---
### align-self
This property aligns a grid item inside a cell along the column axis. This value applies to the content inside a single grid item.
```css
.item {
  align-self: start | end | center | stretch;
}
```
Values:

**start** – aligns the grid item with the start edge of the cell.

**end** – aligns the grid item with the end edge of the cell.

**center** – aligns the grid item in the center of the cell.

**stretch** – (default) fills the whole height of the grid cell.

Example 1:
```css
.item-a {
  align-self: start;
}
```
<img src="https://css-tricks.com/wp-content/uploads/2018/11/align-self-start.svg" width=40%>

Example 2:
```css
.item-a {
  align-self: end;
}
```
<img src="https://css-tricks.com/wp-content/uploads/2018/11/align-self-end.svg" width="40%">

Example 3
```css
.item-a {
  align-self: center;
}
```
<img src=https://css-tricks.com/wp-content/uploads/2018/11/align-self-center.svg width="40%" height="auto">

---
## **CSS Flexbox vs CSS Grid**
CSS flexbox Layout is an one dimension layout so it can work on row or column one at a time but CSS Grid based layout is a two dimension layout so working in both row and columns is possible at a time. Hence we can say that grid based layout systems have better user experience. 

---
### References
[w3schools](https://www.w3schools.com/css/default.asp)

[css-tricks](https://css-tricks.com/snippets)

[mozila-web-docs](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Grid_Layout)
